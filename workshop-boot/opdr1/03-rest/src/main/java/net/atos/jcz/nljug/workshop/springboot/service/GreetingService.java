package net.atos.jcz.nljug.workshop.springboot.service;

import net.atos.jcz.nljug.workshop.springboot.domain.Greeting;

/**
 * The Greeting Service
 *
 * @author Atos
 */
public interface GreetingService {

    Greeting bounce(Greeting greeting);

}
