package net.atos.jcz.nljug.workshop.springboot;

/**
 * Spring boot application.
 *
 * @author Atos
 */
public class Application {

    /**
     * The main() method uses Spring Boot’s SpringApplication.run() method to launch an application. Did you notice that there wasn’t a single line of XML?
     * No web.xml file either. This web application is 100% pure Java and you didn’t have to deal with configuring any plumbing or infrastructure.
     *
     * @param args
     * @throws Exception
     */
    public static void main(final String[] args) throws Exception {
        // implement main method to boot the application.
    }

}
