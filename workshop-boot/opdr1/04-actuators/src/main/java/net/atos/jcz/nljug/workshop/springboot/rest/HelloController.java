package net.atos.jcz.nljug.workshop.springboot.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import net.atos.jcz.nljug.workshop.springboot.domain.Greeting;
import net.atos.jcz.nljug.workshop.springboot.service.GreetingService;

/**
 * Just another hello world example.
 *
 * @author Atos
 */
@RestController // combines the spring-mvc @Controller and @ResponseBody
@RequestMapping("/hello-world")
public class HelloController {

    @Autowired
    private GreetingService greetingService;

    @RequestMapping(method = RequestMethod.GET)
    String helloWorld() {
        return "Hello World!";
    }

    @RequestMapping(value = "/{name}", method = RequestMethod.GET)
    String helloWorld(@PathVariable final String name) {
        return "Hello " + name + "!";
    }

    @RequestMapping(value = "greeting", method = RequestMethod.POST)
    ResponseEntity<Greeting> greeting(@RequestBody final Greeting greeting) {
        return new ResponseEntity<>(greetingService.bounce(greeting), HttpStatus.OK);
    }
}
