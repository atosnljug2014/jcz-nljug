package net.atos.jcz.nljug.workshop.springboot;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

//The @ComponentScan annotation tells Spring to search recursively through the current package and
// its children for classes marked directly or indirectly with Springâ€™s @Component annotation.
// This directive ensures that Spring finds and registers the FotoController,
// because it is marked with @RestController, which in turn is a kind of @Component annotation.

@ComponentScan
//The @EnableAutoConfiguration annotation switches on reasonable default behaviors based on the content of your classpath.
// For example, because the application depends on the embeddable version of Tomcat (tomcat-embed-core.jar), a Tomcat server
// is set up and configured with reasonable defaults on your behalf. And because the application also depends on Spring
// MVC (spring-webmvc.jar), a Spring MVC DispatcherServlet is configured and registered for you â€” no web.xml necessary!
// Auto-configuration is a powerful, flexible mechanism.
@EnableAutoConfiguration

// tags the class as a source of bean definitions for the application context.
@Configuration

/**
 * Spring boot application.
 *
 * @author Atos
 */
public class Application {


    /**
     * The main() method uses Spring Bootâ€™s SpringApplication.run() method to launch an application. Did you notice that there wasnâ€™t a single line of XML?
     * No web.xml file either. This web application is 100% pure Java and you didnâ€™t have to deal with configuring any plumbing or infrastructure.
     *
     * @param args
     * @throws Exception
     */
    public static void main(final String[] args) throws Exception {

        //SpringApplication.run(Application.class, args);

        // fluent API example
        new SpringApplicationBuilder()
                .showBanner(true)
                .sources(Application.class)
                .run(args);
    }

}
