package net.atos.jcz.nljug.workshop.springboot;

import org.junit.Before;
import org.springframework.beans.factory.annotation.Value;

/**
 * @author Atos
 */
public class HelloSpringBootBaseTest {

    // The @Value("${local.server.port}) will be resolved to the actual port number that is used.
    @Value("${local.server.port}")
    private int port;

    protected String baseUrl;

    @Before
    public void setUp() {
        baseUrl = "http://localhost:" + port + "/hello-world";
    }

}
