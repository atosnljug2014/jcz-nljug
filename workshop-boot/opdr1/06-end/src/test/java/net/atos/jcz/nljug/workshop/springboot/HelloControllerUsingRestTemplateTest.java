package net.atos.jcz.nljug.workshop.springboot;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import net.atos.jcz.nljug.workshop.springboot.domain.Greeting;

/**
 * @author Atos
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class HelloControllerUsingRestTemplateTest extends HelloSpringBootBaseTest {


    @Test
    public void thatGetOnHelloWorldReturnsHelloWorldResponse() {
        //arrange
        final RestTemplate template = new RestTemplate();

        //act
        final ResponseEntity<String> responseEntity = template.getForEntity(baseUrl, String.class);

        //assert
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        final String response = responseEntity.getBody();
        assertEquals("Hello World!", response);
    }

    @Test
    public void thatGetOnHelloWorldWithNameReturnsHelloNameResponse() {
        //arrange
        final RestTemplate template = new RestTemplate();

        //act
        final ResponseEntity<String> responseEntity = template.getForEntity(baseUrl + "/JFall", String.class);

        //assert
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        final String response = responseEntity.getBody();
        assertEquals("Hello JFall!", response);
    }

    @Test
    public void thatBounceAGreetingReturnsGreeting() {
        //arrange
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        final RestTemplate template = new RestTemplate();

        final String greetingJSON = "{ \"name\": \"Pietje\", \"message\" : \"Hallo wereld!\" }";

        final HttpEntity<String> requestEntity = new HttpEntity<>(greetingJSON, headers);

        //act
        final ResponseEntity<Greeting> responseEntity = template.postForEntity(baseUrl + "/greeting", requestEntity, Greeting.class);

        //assert
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        final Greeting response = responseEntity.getBody();
        assertEquals("Hallo wereld!", response.getMessage());
        assertEquals("Pietje", response.getName());
    }

}
