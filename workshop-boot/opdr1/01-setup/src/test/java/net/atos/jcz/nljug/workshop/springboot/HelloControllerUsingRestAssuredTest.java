package net.atos.jcz.nljug.workshop.springboot;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.RestAssured.when;

import org.hamcrest.Matchers;
import org.junit.Test;

import com.jayway.restassured.http.ContentType;

/**
 * @author Atos
 */
public class HelloControllerUsingRestAssuredTest extends HelloSpringBootBaseTest {

    @Test
    public void thatGetOnHelloWorldReturnsHelloWorldResponse() {
        when().get(baseUrl).then().statusCode(org.apache.http.HttpStatus.SC_OK).body(Matchers.is("Hello World!"));
    }

    @Test
    public void thatGetOnHelloWorldWithNameReturnsHelloNameResponse() {
        final String name = "Pietje";

        when().get(baseUrl + "/" + name).then().statusCode(org.apache.http.HttpStatus.SC_OK).body(Matchers.is("Hello Pietje!"));
    }

    @Test
    public void thatBounceAGreetingReturnsGreeting() {

        final String greetingJSON = "{ \"name\": \"Pietje\", \"message\" : \"Hallo wereld!\" }";

        given().contentType(ContentType.JSON).body(greetingJSON).when().
            post(baseUrl + "/greeting").then().statusCode(org.apache.http.HttpStatus.SC_OK).
                   body("name", Matchers.is("Pietje")).body("message", Matchers.is("Hallo wereld!"));
    }

}









