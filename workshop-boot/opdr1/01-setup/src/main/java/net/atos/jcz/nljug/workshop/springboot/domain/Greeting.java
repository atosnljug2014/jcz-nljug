package net.atos.jcz.nljug.workshop.springboot.domain;

/**
 * A simple class representing a Greeting
 *
 * @author Atos
 */
public class Greeting {

    private String name;
    private String message;


    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(final String message) {
        this.message = message;
    }


}
