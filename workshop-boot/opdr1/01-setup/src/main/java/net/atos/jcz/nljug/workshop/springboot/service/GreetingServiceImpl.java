package net.atos.jcz.nljug.workshop.springboot.service;

import org.springframework.stereotype.Service;

import net.atos.jcz.nljug.workshop.springboot.domain.Greeting;

/**
 * @author Atos
 */
@Service
public class GreetingServiceImpl implements GreetingService {

    public Greeting bounce(final Greeting greeting) {
        return greeting;
    }

}
