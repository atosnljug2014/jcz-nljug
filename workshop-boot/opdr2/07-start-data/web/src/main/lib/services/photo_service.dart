part of phototagger;

@Injectable()
class PhotoService {

  Http _http;

  PhotoService(this._http);

  /**
   * Load the thumbnails
   */
  Future<List<Thumbnail>> thumbnails(int page, int pageSize, {List<String> tagfilter}) {
    var url = Constants.PHOTO_SERVICE_BASE_URL + "?startPage=$page&pageSize=$pageSize";

    if (tagfilter != null && tagfilter.isNotEmpty) {
      String tagfilterAsString = tagfilter.join(",");
      url += "&tags=$tagfilterAsString";
    }
    print ("Loading using $url");

    return _http.get(url)
      .then((HttpResponse response) => response.data)
      .then((responseData) => responseData.map(_parseThumbnail).toList());
  }

  Thumbnail _parseThumbnail(map) => new Thumbnail(map["id"], map["name"]);

  /**
   * Load the tags
   */
  Future<List<Tag>> tags() =>
    _http.get(Constants.PHOTO_SERVICE_BASE_URL + "/tags")
         .then((HttpResponse response) => response.data)
         .then((responseData) => responseData.map(_parseTag).toList());


  Tag _parseTag(map) => new Tag(map["name"]);

  /** Get the photo by its id */
  Future<Photo> photo(int id) =>
      _http.get(Constants.PHOTO_SERVICE_BASE_URL + "/$id")
           .then((HttpResponse response) => _parsePhoto(response.data));

  Photo _parsePhoto(data) => new Photo(data["id"], data["name"], data["tags"].toList(), new DateTime.fromMillisecondsSinceEpoch(data["createTime"]));

  /**
   * Add tag to photo
   */
  Future<HttpRequest> tagPhoto(int photoId, String tag) =>
  HttpRequest.request(Constants.PHOTO_SERVICE_BASE_URL + "/$photoId/tag/$tag", method: 'PUT')
  .then((HttpResponse response) {
    print("Status: ${response.status}");
    print("Photo tagged");
  });

  /**
   * Upload a photo
   */
  Future<HttpRequest> uploadPhoto(FormData data) =>
  HttpRequest.request(Constants.PHOTO_SERVICE_BASE_URL, method: 'POST', sendData : data)
  .then((HttpResponse response) {
    print("Status: ${response.status}");
    print("Photo uploaded succesfully");
  });

}