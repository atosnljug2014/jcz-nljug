part of phototagger;

@Component(selector: 'thumbnails',
          publishAs: 'thumbnailsComp',
        templateUrl: 'lib/components/thumbnails_component.html',
       useShadowDom: false)
class ThumbnailsComponent {

  /*** start constants ***/
  static const String ERROR_MESSAGE = "Sorry! Loading the photos failed";
  static const String LOADING_MESSAGE = "Loading...";
  static const int PAGE_SIZE = 9;
  static const int LOAD_SCROLL_HEIGHT = 10;
  /*** end constants ***/

  /*** start private variables ***/
  List<Thumbnail> _thumbnails = new List();
  PhotoService _photoService;
  String _message = LOADING_MESSAGE;
  bool _thumbnailsLoaded = false;

  //counter for the page to load
  int _page = 0;
  bool _attached = false;
  //counter for the last reached scroll position
  int _lastScrollPosition = 0;
  /*** end private variables ***/

  /**
   * Constructor which will load the first batch of thumbnails
   *
   * @param service the photo service
   */
  ThumbnailsComponent(PhotoService service) {
    this._photoService = service;
    _loadThumbnails();
  }

  /**
   * Initially loads the thumbnails
   *
   * @param initalLoading when true it will attach the scrolllistener
   * @param tagfilter optional parameter for filtering based on the tagname
   */
  void _loadThumbnails() {
    print("initial loading page $_page");
    _photoService.thumbnails(_page, PAGE_SIZE)
      .then((thumbnails) {
        _thumbnails.addAll(thumbnails);
        _thumbnailsLoaded = true;
        print("done loading page $_page");
      }).catchError(handleError);
  }

  /**
   * Loads the thumbnails for a specified tag
   *
   * @param tagfilter parameter for filtering based on the tagname
   */
  void _loadThumbnailsForTags(List<String> tagfilter) {
    print("loading page $_page with filter $tagfilter");
    _photoService.thumbnails(_page, PAGE_SIZE, tagfilter: tagfilter)
      .then((thumbnails) {
        _thumbnails.clear();
        _thumbnails.addAll(thumbnails);
        print("done loading page $_page with filter $tagfilter");
      }).catchError(handleError);
  }

  void handleError(Exception e) {
    _thumbnailsLoaded = false;
    _message = ERROR_MESSAGE;
  }

  /**
   * The scroll listener to the container which has the style 'overflow-y: scroll' set.
   * Only when a certain point has been reached for the first time load the next set of data
   */
  void scroll() {
    DivElement thumbnailsContainer = querySelector('#thumbnailContainer');
    if (thumbnailsContainer.scrollTop % LOAD_SCROLL_HEIGHT == 0 && thumbnailsContainer.scrollTop > _lastScrollPosition) {
      _lastScrollPosition = thumbnailsContainer.scrollTop;
      _page++;
      _loadThumbnails();
    }
  }

  void toggleTags() {
    DivElement rightContainer = querySelector('.rightContainer');
    DivElement leftContainer = querySelector('.leftContainer');
    rightContainer.classes.toggle('rightContainerSmall');
    leftContainer.classes.toggle('leftContainerShow');
  }

  /*** start getters and setters ***/
  List<Thumbnail> get thumbnails => _thumbnails;

  String get message => _message;

  bool get thumbnailsLoaded => _thumbnailsLoaded;

  String get serverUrl => Constants.PHOTO_SERVICE_BASE_URL;
  /*** end getters and setters ***/

}

