part of phototagger;

@Component(selector: 'tags', publishAs: 'tagsComp', templateUrl: 'lib/components/tags_component.html', useShadowDom: false)
class TagsComponent {

  /*** start constants ***/
  static const String ERROR_MESSAGE = "Sorry! Loading the tags failed";
  static const String LOADING_MESSAGE = "Loading...";

  /*** end constants ***/

  /*** start private variables ***/
  List<Tag> _tags = new List();

  PhotoService _photoService;

  ThumbnailsComponent _thumbnailsComponent;

  String _message = LOADING_MESSAGE;

  String _nameFilterString = "";

  List<String> _tagNames = [];
  /*** end private variables ***/

  /**
   * Constructor which will load the tags
   *
   * @param service the photo service
   * @param thumbnailsComponent the thumbnails component
   */
  TagsComponent(PhotoService service, ThumbnailsComponent thumbnailsComponent) {
    this._photoService = service;
    this._thumbnailsComponent = thumbnailsComponent;
    _loadTags();
  }

  /**
   * Loads the tags from the photo service
   */
  _loadTags() {
    _photoService.tags().then((tags) => _tags.addAll(tags)).then((tags) => print("done loading tags")).catchError((e) => _message = ERROR_MESSAGE);
  }

  selectTag(Tag tag) {
    if (tag.selected) {
      _tagNames.remove(tag.name);
      tag.selected = false;
    } else {
      _tagNames.add(tag.name);
      tag.selected = true;
    }
    _thumbnailsComponent._loadThumbnailsForTags(_tagNames);
  }

  /*** start getters and setters ***/
  List<Tag> get tags => _tags;

  String get message => _message;

  String get nameFilterString => _nameFilterString;

  void set nameFilterString(String nameFilterString) {
    _nameFilterString = nameFilterString;
  }
  /*** end getters and setters ***/

}

