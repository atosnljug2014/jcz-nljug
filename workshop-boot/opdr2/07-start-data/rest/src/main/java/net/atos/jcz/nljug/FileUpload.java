package net.atos.jcz.nljug;

import javax.servlet.MultipartConfigElement;

import org.springframework.boot.context.embedded.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Atos
 */
@Configuration
public class FileUpload {

    @Bean
    MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        factory.setMaxFileSize("10000000KB");
        factory.setMaxRequestSize("1000000KB");
        return factory.createMultipartConfig();
    }
}
