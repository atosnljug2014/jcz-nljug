package net.atos.jcz.nljug.rest.photo;

import java.io.Serializable;
import java.util.Date;
import java.util.stream.Collectors;

import net.atos.jcz.nljug.rest.RestObject;
import net.atos.jcz.nljug.service.Tag;

/**
 * @author Atos
 */
public class Photo extends RestObject implements Serializable {

    private static final long serialVersionUID = 7174906541551115987L;

    private String id;
    private String name;
    private String[] tags;
    private Date createTime;

    public Photo() {
       super();
    }

    public Photo(final net.atos.jcz.nljug.service.Photo photo) {
        this();
        this.id = photo.getId();
        this.name = photo.getName();
        this.createTime = photo.getCreateTime();

        if (photo.getTags() != null) {
            this.tags = photo.getTags().parallelStream().map(Tag::getName).collect(Collectors.toList()).toArray(new String[photo.getTags().size()]);
        }
        super.setResource("/photos/" + id);
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(final Date createTime) {
        this.createTime = createTime;
    }

    public String[] getTags() {
        return tags;
    }

    public void setTags(final String[] tags) {
        this.tags = tags;
    }
}
