package net.atos.jcz.nljug.rest.photo;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Range;
import com.google.common.primitives.Ints;

import net.atos.jcz.nljug.service.PhotoService;
import net.atos.jcz.nljug.service.SearchParameters;
import net.atos.jcz.nljug.service.Tag;

/**
 * @author Atos
 */
@EnableAutoConfiguration
@RestController
@RequestMapping("/photos")
public class PhotosController {

    private static final String PAGE_SIZE_DEFAULT = "100";

    private static final Integer PAGE_SIZE_MAX = 1000;

    private static final String ZERO = "0";

    private static final String DEFAULT_THUMBNAIL_SIZE = "100";

    @Autowired
    private PhotoService photoService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Photo> getPhotos(@RequestParam(required = false, defaultValue = ZERO) final Integer startPage,
                                 @RequestParam(required = false, defaultValue = PAGE_SIZE_DEFAULT) final Integer pageSize,
                                 @RequestParam(required = false) final List<String> tags) {
        if (!Range.closed(Integer.valueOf(ZERO), PAGE_SIZE_MAX).contains(pageSize)) {
            throw new IllegalArgumentException();
        }
        if (tags == null) {
            return photoService.getAllPhotos(new PageRequest(startPage, pageSize)).getContent().stream().map(Photo::new).collect(Collectors.toList());
        } else {
            final SearchParameters searchParameters = new SearchParameters(tags.stream().map(Tag::new).collect(Collectors.toSet()));
            return photoService.findPhotos(new PageRequest(startPage, pageSize), searchParameters).getContent().stream().map(Photo::new).collect(
                    Collectors.toList());
        }
    }

    @RequestMapping(value = "/tags", method = RequestMethod.GET)
    public List<Tag> getPhotoTags() {
        return photoService.getAllPhotoTags();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Photo getPhoto(@PathVariable @NotNull final String id) {
        return new Photo(photoService.getPhoto(id));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deletePhoto(@PathVariable @NotNull final String id) {
        photoService.deletePhoto(id);
    }

    @ResponseBody
    @RequestMapping(value = "/{id}/thumb", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] getPhotoThumb(@PathVariable final String id,
                                @RequestParam(required = false, defaultValue = DEFAULT_THUMBNAIL_SIZE, value = "width")
                                final String width,
                                @RequestParam(required = false, defaultValue = DEFAULT_THUMBNAIL_SIZE, value = "height")
                                final String height) {

        final Integer iWidth = Ints.tryParse(width);
        final Integer iHeight = Ints.tryParse(height);

        if (iWidth == null || iHeight == null) {
            throw new IllegalArgumentException();
        }

        return photoService.getPhotoBinary(id, iWidth, iHeight);
    }

    @RequestMapping(value = "/{id}/tag/{tagname}", method = RequestMethod.PUT)
    public void tagPhoto(@PathVariable final String id, @PathVariable final String tagname) {
        photoService.tagPhoto(id, new Tag(tagname));
    }

    @ResponseBody
    @RequestMapping(value = "/{id}/content", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
    // TODO, iets anders qua mediatype?
    public byte[] getPhotoContent(@PathVariable final String id) {
        return photoService.getPhotoBinary(id);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Photo addPhoto(@RequestParam(required = true) final String name,
                          @RequestParam(value = "content", required = true) final MultipartFile file,
                          final HttpServletRequest httpServletRequest) {

        if (file.isEmpty()) {
            throw new IllegalArgumentException();
        }

        try {
            final byte[] bytes = file.getBytes();

            final net.atos.jcz.nljug.service.Photo photoEntity = new net.atos.jcz.nljug.service.Photo(name);
            photoEntity.setCreateTime(new Date());

            final Photo addedPhoto = new Photo(photoService.addPhoto(photoEntity));
            addedPhoto.setResource(httpServletRequest.getServletPath() + "/photos/" + addedPhoto.getId());

            photoService.addPhotoBinary(addedPhoto.getId(), bytes);

            return addedPhoto;

        } catch (final IOException e) {
            throw new RuntimeException();
        }
    }

}
