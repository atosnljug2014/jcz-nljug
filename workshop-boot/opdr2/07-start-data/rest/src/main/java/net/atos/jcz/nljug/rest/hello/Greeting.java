package net.atos.jcz.nljug.rest.hello;

/**
 * @author Atos
 */
public class Greeting {

    private String name;
    private String message;


    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(final String message) {
        this.message = message;
    }
}
