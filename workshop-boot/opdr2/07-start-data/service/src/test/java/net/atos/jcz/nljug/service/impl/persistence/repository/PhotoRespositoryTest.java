package net.atos.jcz.nljug.service.impl.persistence.repository;

import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.IMongodConfig;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.runtime.Network;
import net.atos.jcz.nljug.service.Tag;
import net.atos.jcz.nljug.service.configuration.MongoConfiguration;
import net.atos.jcz.nljug.service.impl.persistence.entity.PhotoEntity;
import net.atos.jcz.nljug.service.impl.persistence.entity.TagEntity;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;

/**
 * @author Atos
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MongoConfiguration.class)
public class PhotoRespositoryTest {

    @Autowired
    private PhotoRepository photoRepository;
    private static MongodExecutable mongodExecutable;

    @BeforeClass
    public static void initializeDB() throws IOException {
        MongodStarter starter = MongodStarter.getDefaultInstance();
        int port = 27017;


        final String userHome = System.getProperty("user.home");

        final Path targetPath = Paths.get(userHome, ".embedmongo", "linux");

        FileUtils.deleteDirectory(targetPath.toFile());
        Files.createDirectories(targetPath);

        Files.copy(PhotoRespositoryTest.class.getResourceAsStream("/mongodb-linux-x86_64-2.6.1.tgz"),
                Paths.get(targetPath.toString(), "mongodb-linux-x86_64-2.6.1.tgz"));

        // Download mongo via http://fastdl.mongodb.org/linux/mongodb-linux-x86_64-2.6.1.tgz
        // en plaats deze in ~/.embedmongo/linux als het spul niet gedownload wil worden vanwege proxy problemen
        final IMongodConfig mongodConfig = new MongodConfigBuilder()
                .version(Version.Main.V2_6)
                .net(new Net(port, Network.localhostIsIPv6()))
                .build();

        mongodExecutable = starter.prepare(mongodConfig);
        mongodExecutable.start();
    }

    @AfterClass
    public static void shutdownDB() throws InterruptedException {
        mongodExecutable.stop();
    }

    @After
    public void tearDown() throws Exception {
        photoRepository.deleteAll();
    }

    @Test
    public void testSavePhotoEntity() {
        // Arrange
        final PhotoEntity unsavedEntity = new PhotoEntity("photoname");
        unsavedEntity.setTags(new HashSet<>(Arrays.asList(new TagEntity("tag1"), new TagEntity("tag2"))));

        // Act
        photoRepository.save(unsavedEntity);

        // Assert
        int photosInRepository = photoRepository.findAll().size();
        Assert.assertEquals(1, photosInRepository);

        final PhotoEntity savedEntity = photoRepository.findAll().get(0);
        Assert.assertNotNull(savedEntity);
        Assert.assertNotNull(savedEntity.getTags());
        Assert.assertEquals(2, savedEntity.getTags().size());
    }
}

