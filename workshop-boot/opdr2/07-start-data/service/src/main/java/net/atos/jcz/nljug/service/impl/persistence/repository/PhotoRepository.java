package net.atos.jcz.nljug.service.impl.persistence.repository;

import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import net.atos.jcz.nljug.service.impl.persistence.entity.PhotoEntity;
import net.atos.jcz.nljug.service.impl.persistence.entity.TagEntity;

/**
 * @author Atos
 */
public interface PhotoRepository extends MongoRepository<PhotoEntity, String> {

    // Werkt automagically via property expressions
    // http://docs.spring.io/spring-data/mongodb/docs/current/reference/html/#repositories.query-methods.query-property-expressions
    Page<PhotoEntity> findByTagsIn(Set<TagEntity> tags, Pageable pageable);

}