package net.atos.jcz.nljug.service.impl.persistence.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import net.atos.jcz.nljug.service.impl.persistence.entity.TagEntity;

/**
 * @author Atos
 */
public interface TagRepository extends MongoRepository<TagEntity, String> {

}