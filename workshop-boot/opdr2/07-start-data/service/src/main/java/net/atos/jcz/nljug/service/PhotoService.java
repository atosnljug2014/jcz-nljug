package net.atos.jcz.nljug.service;

import java.util.Collection;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @author Atos
 */
public interface PhotoService {

    Photo addPhoto(Photo photo);

    /**
     * Retrieves all photos, given {@link Pageable} parameters.
     *
     * @param pageable - An instance of {@link Pageable} containing paging and sorting parameters. This parameter may
     *                 not be null.
     * @return - A {@link java.util.Collection} of {@link Photo} objects. Will not return null, but an empty {@link java.util.Collection}
     * when no {@link Photo}s are found
     */
    Page<Photo> getAllPhotos(Pageable pageable);

    /**
     * Get photo based on id.
     *
     * @param photoID - The id of the photo.
     * @return The {@link Photo} with id, in case the photo not exists null.
     */
    Photo getPhoto(String photoID);

    /**
     * Deletes the photo and its binary content
     *
     * @param photoID - The ID of the photo to delete
     */
    void deletePhoto(String photoID);

    /**
     * Searches photos, given the {@link Pageable} and {@link SearchParameters}.
     *
     * @param pageable         - An instance of {@link Pageable} containing paging and sorting parameters. This
     *                         parameter may not be null.
     * @param searchParameters - An instance of {@link SearchParameters} containing all search requirements. This
     *                         parameter may not be  null.
     * @return - A {@link java.util.Collection} of {@link Photo} objects matching the given {@link SearchParameters}. Will not
     * return null, but an empty {@link java.util.Collection} when no {@link Photo}s are found
     */
    Page<Photo> findPhotos(Pageable pageable, SearchParameters searchParameters);

    /**
     * Retrieves the original binary photo for the given ID.
     *
     * @param photoID - The ID to retrieve the original binary photo for.
     * @return - A byte array containing the binary photo data. Will return null if no photo binary is found for the
     * given ID
     */
    byte[] getPhotoBinary(String photoID);

    /**
     * Retrieves a resized version of the binary photo for the given ID. Clients may determine their own photo size. The
     * photo binary will be resized, respecting the aspect ratio of the original photo. So it is guaranteed that the
     * returned photo binary, will not be larger than the given width and height. It is however not guaranteed that the
     * returned photo binary is actually exactly the size corresponding to the given width and height, as that depends
     * on the aspect ratio of the photo.
     *
     * @param photoID - The ID to retrieve the original binary photo for.
     * @return - A byte array containing the binary photo data. Will return null if no photo binary is found for the
     * given ID
     */
    byte[] getPhotoBinary(String photoID, int width, int height);

    /**
     * Adds a binary photo for the given photoID. If no photo is found for the given ID, the binary will not be saved.
     * If a binary already exists for the given photoID, the existing binary will be overridden for the given photo
     * binary.
     *
     * @param photoID - The ID of the photo to save the binary photo for.
     * @param bytes   - A byte array containing the binary photo data.
     */
    void addPhotoBinary(String photoID, byte[] bytes);

    /**
     * Retrieves all photo tags
     *
     * @return - A {@link java.util.Collection} of {@link Tag} objects. Will not return null, but an empty {@link java.util.Collection}
     * when no {@link Tag}s are found
     */
    List<Tag> getAllPhotoTags();

    /**
     * Tags a photo.
     *
     * @param photoID - The id of the photo.
     * @param tag     - The tag
     */
    void tagPhoto(String photoID, Tag tag);
}
