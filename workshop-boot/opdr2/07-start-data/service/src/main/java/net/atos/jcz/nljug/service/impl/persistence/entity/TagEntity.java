package net.atos.jcz.nljug.service.impl.persistence.entity;

import org.springframework.data.annotation.Id;

/**
 * @author Atos
 */
public class TagEntity {

    @Id
    private String name;

    public TagEntity(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final TagEntity tagEntity = (TagEntity) o;

        if (name != null ? !name.equals(tagEntity.name) : tagEntity.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }
}
