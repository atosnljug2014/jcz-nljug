package net.atos.jcz.nljug.rest.jersey.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Component;


@Path("/hello-world")
@Component
public class HelloResource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/")
    public String helloWorld() {
        return "Hello World!";
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{name}")
    public String helloMe(@PathParam("name") final String name) {
        return "Hello " + name + "!";
    }

}