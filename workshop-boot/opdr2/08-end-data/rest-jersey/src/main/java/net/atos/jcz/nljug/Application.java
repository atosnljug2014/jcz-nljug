package net.atos.jcz.nljug;

import org.glassfish.jersey.servlet.ServletContainer;
import org.glassfish.jersey.servlet.ServletProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import net.atos.jcz.nljug.rest.jersey.config.JerseyConfig;

//The @ComponentScan annotation tells Spring to search recursively through the current package and
// its children for classes marked directly or indirectly with Spring’s @Component annotation.
// This directive ensures that Spring finds and registers the FotoController,
// because it is marked with @RestController, which in turn is a kind of @Component annotation.

@ComponentScan
//The @EnableAutoConfiguration annotation switches on reasonable default behaviors based on the content of your classpath.
// For example, because the application depends on the embeddable version of Tomcat (tomcat-embed-core.jar), a Tomcat server
// is set up and configured with reasonable defaults on your behalf. And because the application also depends on Spring
// MVC (spring-webmvc.jar), a Spring MVC DispatcherServlet is configured and registered for you — no web.xml necessary!
// Auto-configuration is a powerful, flexible mechanism.
@EnableAutoConfiguration

// tags the class as a source of bean definitions for the application context.
@Configuration
/**
 * Spring boot application.
 *
 * @author Atos
 */
public class Application {


    /**
     * The main() method uses Spring Boot’s SpringApplication.run() method to launch an application. Did you notice that there wasn’t a single line of XML?
     * No web.xml file either. This web application is 100% pure Java and you didn’t have to deal with configuring any plumbing or infrastructure.
     *
     * see src/main/resources/config/application.properties for some configuration.
     *
     * @param args
     * @throws Exception
     */
    public static void main(final String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }

    /**
     * Register the jerseyServlet to the container
     *
     */
    @Bean
    public ServletRegistrationBean jerseyServlet() {
        ServletRegistrationBean registration = new ServletRegistrationBean(new ServletContainer(), "/rest/*");
        registration.addInitParameter(ServletProperties.JAXRS_APPLICATION_CLASS, JerseyConfig.class.getName());
        return registration;
    }
}
