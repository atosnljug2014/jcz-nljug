package net.atos.jcz.nljug.rest.jersey.resources;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import net.atos.jcz.nljug.service.PhotoService;


@Path("/photo")
@Component
public class PhotoResource {

    private static final String PAGE_SIZE_DEFAULT = "100";
    private static final Integer PAGE_SIZE_MAX = 1000;
    private static final String ZERO = "0";


    @Autowired
    private PhotoService photoService;

    @Context
    private HttpServletResponse response;

//    @GET
//    @Produces(MediaType.APPLICATION_JSON)
//    public Response getPhotos(@DefaultValue(ZERO) @QueryParam("startPage") final Integer startPage,
//                                 @DefaultValue(PAGE_SIZE_DEFAULT) @QueryParam("pageSize") final Integer pageSize) {
//        return Response.ok(wrapper)
//                       .header("Access-Control-Allow-Origin", "*").build();
//
//    }

}