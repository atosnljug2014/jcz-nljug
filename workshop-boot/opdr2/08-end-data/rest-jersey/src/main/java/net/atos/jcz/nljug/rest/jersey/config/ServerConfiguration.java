package net.atos.jcz.nljug.rest.jersey.config;

import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Class verifies "server.port" is set.
 *
 * @author Atos
 */
@Component
@ConfigurationProperties(prefix = "server")
public class ServerConfiguration {

    @NotNull
    private String port;

    public String getPort() {
        return port;
    }

    public void setPort(final String port) {
        this.port = port;
    }
}
