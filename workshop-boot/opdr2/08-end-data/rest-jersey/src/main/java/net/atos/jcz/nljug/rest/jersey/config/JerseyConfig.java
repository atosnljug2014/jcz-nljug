package net.atos.jcz.nljug.rest.jersey.config;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.spring.scope.RequestContextFilter;


/**
 * Jersey configuration (jersey spring extenstion).
 *
 * see https://jersey.java.net/documentation/latest/spring.html
 *
 * @author Atos
 */
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        // Mandatory configuration.
        register(RequestContextFilter.class);
        // package containing jax-rs resources
        packages("net.atos.jcz.nljug.rest.jersey.resources");

        // register the logging filter for logging the request and response headers.
        // register(LoggingFilter.class);
    }
}