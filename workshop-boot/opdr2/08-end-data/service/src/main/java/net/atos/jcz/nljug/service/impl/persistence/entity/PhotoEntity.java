package net.atos.jcz.nljug.service.impl.persistence.entity;

import java.util.Date;
import java.util.Set;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

/**
 * @author Atos
 */
public class PhotoEntity {

    @Id
    private String id;

    private String name;

    private Date createTime;

    @DBRef
    private Set<TagEntity> tags;

    public PhotoEntity(final String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public Set<TagEntity> getTags() {
        return tags;
    }

    public void setTags(final Set<TagEntity> tags) {
        this.tags = tags;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(final Date createTime) {
        this.createTime = createTime;
    }
}
