package net.atos.jcz.nljug.service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Atos
 */
public class SearchParameters {

    private Set<Tag> tags = new HashSet<>();

    public SearchParameters(final Tag... tags) {
        this.tags.addAll(Arrays.asList(tags));
    }

    public SearchParameters(final Set<Tag> tags) {
        this.tags = tags;
    }

    public Set<Tag> getTags() {
        return tags;
    }
}
