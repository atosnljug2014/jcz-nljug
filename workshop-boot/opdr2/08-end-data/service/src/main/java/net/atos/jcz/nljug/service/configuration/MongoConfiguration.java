package net.atos.jcz.nljug.service.configuration;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import net.atos.jcz.nljug.service.PhotoService;
import net.atos.jcz.nljug.service.impl.persistence.repository.PhotoRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@ComponentScan(basePackageClasses = PhotoService.class)
@EnableAutoConfiguration
@EnableMongoRepositories(basePackageClasses = {PhotoService.class})
public class MongoConfiguration extends AbstractMongoConfiguration {

    @Value("${mongodb.host}")
    private String host;

    @Value("${mongodb.port}")
    private int port;

    @Value("${mongodb.databasename}")
    private String databaseName;

    @Override
    protected String getDatabaseName() {
        return databaseName;
    }

    @Override
    public Mongo mongo() throws Exception {
        return new MongoClient(host, port);
    }

    @Override
    protected String getMappingBasePackage() {
        return "net.atos.jcz.nljug.service.impl.persistence.entity";
    }
}