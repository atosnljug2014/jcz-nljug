package net.atos.jcz.nljug.service.impl;


import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.google.common.io.ByteStreams;

import net.atos.jcz.nljug.service.Photo;
import net.atos.jcz.nljug.service.PhotoService;
import net.atos.jcz.nljug.service.SearchParameters;
import net.atos.jcz.nljug.service.Tag;
import net.atos.jcz.nljug.service.impl.persistence.entity.PhotoEntity;
import net.atos.jcz.nljug.service.impl.persistence.entity.TagEntity;
import net.atos.jcz.nljug.service.impl.persistence.repository.PhotoRepository;
import net.atos.jcz.nljug.service.impl.persistence.repository.TagRepository;

/**
 * @author Atos
 */
@Service
@Transactional
public class DefaultPhotoService implements PhotoService {

    public static final String JPG = "jpg";

    @Autowired
    private PhotoRepository photoRepository;

    @Autowired
    private TagRepository tagRepository;

    @Value("${photoBinariesDirectory}")
    private String photoBinariesDirectoryString;

    private Path photoBinariesDirectory;

    @PostConstruct
    private void init() throws IOException {
        photoBinariesDirectory = Paths.get(photoBinariesDirectoryString);

        if (!Files.exists(photoBinariesDirectory)) {
            Files.createDirectories(photoBinariesDirectory);
        }

        if (!Files.isDirectory(photoBinariesDirectory)) {
            throw new IllegalStateException(
                    String.format("Cannot save photo binaries to '%s', as it is not a directory",
                            photoBinariesDirectoryString));
        }

        Files.createDirectories(photoBinariesDirectory);
    }

    @Override
    public Photo addPhoto(final Photo photo) {
        final Set<TagEntity> tagEntities = convertToTagEntities(photo.getTags());
        tagRepository.save(tagEntities);

        final PhotoEntity savedEntity = photoRepository.save(convertToPhotoEntity(photo));
        return convertToPhoto(savedEntity);
    }

    @Override
    public Page<Photo> getAllPhotos(final Pageable pageable) {
        final Page<PhotoEntity> photoEntities = photoRepository.findAll(pageable);
        final List<Photo> photos = photoEntities.getContent().parallelStream().map(this::convertToPhoto).collect(Collectors.toList());
        return new PageImpl<>(photos, pageable, photoEntities.getTotalElements());
    }

    @Override
    public List<Tag> getAllPhotoTags() {
        final List<TagEntity> tagEntities = tagRepository.findAll();
        return tagEntities.parallelStream().map(this::convertToTag).collect(Collectors.toList());
    }

    @Override
    public Photo getPhoto(final String photoID) {
        final PhotoEntity savedEntity = photoRepository.findOne(photoID);
        return convertToPhoto(savedEntity);
    }

    @Override
    public Page<Photo> findPhotos(final Pageable pageable, final SearchParameters searchParameters) {
        final Page<PhotoEntity> photoEntities;

        if (CollectionUtils.isEmpty(searchParameters.getTags())) {
            photoEntities = photoRepository.findAll(pageable);
        } else {
            photoEntities = photoRepository.findByTagsIn(convertToTagEntities(searchParameters.getTags()), pageable);
        }

        final List<Photo> photos = photoEntities.getContent().parallelStream().map(this::convertToPhoto).collect(Collectors.toList());
        return new PageImpl<>(photos, pageable, photoEntities.getTotalElements());
    }

    @Override
    public byte[] getPhotoBinary(final String photoID) {
        final Path photoPath = this.photoBinariesDirectory.resolve(photoID);

        final byte[] photoBytes;
        try {
            // if for some reason the content cannot be found on the filesystem anymore return the 'no image' jpeg
            if (!Files.exists(photoPath)) {
                photoBytes = ByteStreams.toByteArray(this.getClass().getResourceAsStream("/no_image.jpeg"));
            } else {
                final byte[] dataBytes =  Files.readAllBytes(photoPath);
                // if the uploaded photoBytes cannot be converted to a valid photo also return the 'no image' jpeg
                if (ImageIO.read(new ByteArrayInputStream(dataBytes)) == null) {
                    photoBytes = ByteStreams.toByteArray(this.getClass().getResourceAsStream("/no_image.jpeg"));
                } else {
                    photoBytes = dataBytes;
                }
            }

        } catch (final IOException e) {
            throw new IllegalStateException(e.getMessage(), e);
        }

        return photoBytes;
    }

    @Override
    public byte[] getPhotoBinary(final String photoID, final int width, final int height) {
        final byte[] originalPhotoAsBytes = this.getPhotoBinary(photoID);

        if (originalPhotoAsBytes == null) {
            return null;
        }

        try (final ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
            final BufferedImage originalPhoto = ImageIO.read(new ByteArrayInputStream(originalPhotoAsBytes));
            final BufferedImage resizedPhoto = resizeImage(originalPhoto, width, height);

            ImageIO.write(resizedPhoto, JPG, bos);

            return bos.toByteArray();
        } catch (final IOException e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
    }

    @Override
    public void addPhotoBinary(final String photoID, final byte[] bytes) {
        if (!this.photoRepository.exists(photoID)) {
            return;
        }

        final Path photoPath = this.photoBinariesDirectory.resolve(photoID);
        try {
            Files.write(photoPath, bytes);
        } catch (final IOException e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
    }

    @Override
    public void deletePhoto(final String photoID) {
        photoRepository.delete(photoID);

        final Path photoPath = this.photoBinariesDirectory.resolve(photoID);
        try {
            Files.delete(photoPath);
        } catch (final IOException e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
    }

    @Override
    public void tagPhoto(final String photoID, final Tag tag) {
        final TagEntity tagEntity = convertToTagEntity(tag);
        tagRepository.save(tagEntity);

        final PhotoEntity photo = photoRepository.findOne(photoID);
        photo.getTags().add(tagEntity);

        photoRepository.save(photo);
    }

    private Photo convertToPhoto(final PhotoEntity photoEntity) {
        final Photo photo = new Photo(photoEntity.getId(), photoEntity.getName());
        photo.setCreateTime(photoEntity.getCreateTime());
        photo.setTags(photoEntity.getTags().parallelStream().map(t -> new Tag(t.getName())).collect(Collectors.toSet()));
        return photo;
    }

    private Tag convertToTag(final TagEntity tagEntity) {
        return new Tag(tagEntity.getName());
    }

    private TagEntity convertToTagEntity(final Tag tag) {
        return new TagEntity(tag.getName());
    }

    private PhotoEntity convertToPhotoEntity(final Photo photo) {
        final PhotoEntity photoEntity;

        if (photo.getId() == null) {
            photoEntity = new PhotoEntity(ObjectId.get().toString());
        } else {
            photoEntity = new PhotoEntity(photo.getId());
        }

        photoEntity.setName(photo.getName());
        photoEntity.setCreateTime(photo.getCreateTime());

        photoEntity.setTags(convertToTagEntities(photo.getTags()));

        return photoEntity;
    }

    private Set<TagEntity> convertToTagEntities(final Collection<Tag> tags) {
        return tags.parallelStream().map(t -> new TagEntity(t.getName())).collect(Collectors.toSet());
    }

    private static BufferedImage resizeImage(final BufferedImage originalImage, final int width, final int height) {
        final int type = originalImage.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
        final BufferedImage resizedImage = new BufferedImage(width, height, type);
        final Graphics2D g = resizedImage.createGraphics();
        g.drawImage(originalImage, 0, 0, width, height, null);
        g.dispose();

        return resizedImage;
    }
}
