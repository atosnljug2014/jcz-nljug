package net.atos.jcz.nljug.service.impl.persistence.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import net.atos.jcz.nljug.service.impl.persistence.entity.TagEntity;

/**
 * @author Atos
 */
// Annotation to expose the repository via REST.
@RepositoryRestResource(collectionResourceRel = "tags", path = "tagsRepo")
public interface TagRepository extends MongoRepository<TagEntity, String> {

}