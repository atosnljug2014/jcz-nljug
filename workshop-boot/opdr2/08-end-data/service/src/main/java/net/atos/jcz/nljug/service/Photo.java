package net.atos.jcz.nljug.service;

import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Atos
 */
public class Photo {
    private String id;

    private final String name;

    private Date createTime;

    private Set<Tag> tags;

    public Photo(final String name) {
        this.name = name;
    }

    public Photo(final String id, final String name) {
        this(name);
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public Set<Tag> getTags() {
        if (tags == null) {
            tags = new HashSet<>();
        }
        return tags;
    }

    public void setTags(final Set<Tag> tags) {
        this.tags = tags;
    }

    public String getName() {
        return name;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(final Date createTime) {
        this.createTime = createTime;
    }
}
