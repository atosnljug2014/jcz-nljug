package net.atos.jcz.nljug.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.IMongodConfig;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.runtime.Network;
import net.atos.jcz.nljug.service.configuration.MongoConfiguration;
import net.atos.jcz.nljug.service.impl.persistence.entity.TagEntity;
import net.atos.jcz.nljug.service.impl.persistence.repository.PhotoRepository;
import net.atos.jcz.nljug.service.impl.persistence.repository.TagRepository;

/**
 * @author Atos
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MongoConfiguration.class)
public class PhotoServiceTest {

    @Value("${photoBinariesDirectory}")
    private String photoBinariesDirectoryString;

    @Autowired
    private PhotoRepository photoRepository;

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private PhotoService photoService;

    private static MongodExecutable mongodExecutable;

    @BeforeClass
    public static void initializeDB() throws IOException {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        MongodStarter starter = MongodStarter.getDefaultInstance();
        int port = 27017;


        final String userHome = System.getProperty("user.home");

        final Path targetPath = Paths.get(userHome, ".embedmongo", "linux");

        FileUtils.deleteDirectory(targetPath.toFile());
        Files.createDirectories(targetPath);

        Files.copy(PhotoServiceTest.class.getResourceAsStream("/mongodb-linux-x86_64-2.6.1.tgz"),
                Paths.get(targetPath.toString(), "mongodb-linux-x86_64-2.6.1.tgz"));

        // Download mongo via http://fastdl.mongodb.org/linux/mongodb-linux-x86_64-2.6.1.tgz
        // en plaats deze in ~/.embedmongo/linux als het spul niet gedownload wil worden vanwege proxy problemen
        final IMongodConfig mongodConfig = new MongodConfigBuilder()
                .version(Version.Main.V2_6)
                .net(new Net(port, Network.localhostIsIPv6()))
                .build();

        mongodExecutable = starter.prepare(mongodConfig);
        mongodExecutable.start();
    }

    @AfterClass
    public static void shutdownDB() throws InterruptedException {
        mongodExecutable.stop();
    }

    @After
    public void cleanup() throws IOException {
        Files.list(Paths.get(photoBinariesDirectoryString)).forEach(path -> {
            try {
                Files.delete(path);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }

    @After
    public void tearDown() throws Exception {
        photoRepository.deleteAll();
    }

    @Test
    public void addPhotoShouldSavePhotoToPhotoBinariesDirectory() throws IOException {
        // Given
        final Photo savedPhoto = photoService.addPhoto(new Photo("testPhoto"));
        final Path photoBinariesDirectory = Paths.get(photoBinariesDirectoryString);

        // When
        photoService.addPhotoBinary(savedPhoto.getId(),
                IOUtils.toByteArray(PhotoServiceTest.class.getResourceAsStream("/test.png")));

        // Then
        Assert.assertEquals(1, Files.list(photoBinariesDirectory).count());
        Files.list(photoBinariesDirectory).forEach(path -> Assert.assertEquals(
                photoBinariesDirectory.resolve(savedPhoto.getId()), path));
    }

    @Test
    public void findPhotosByTagShouldFindPhotos() throws IOException {
        // Given
        final Photo photoWithTags1 = new Photo("testPhotoWithTags1");
        photoWithTags1.getTags().add(new Tag("firstTag"));
        photoWithTags1.getTags().add(new Tag("secondTag"));
        photoService.addPhoto(photoWithTags1);

        final Photo photoWithTags2 = new Photo("testPhotoWithTags2");
        photoWithTags2.getTags().add(new Tag("thirdTag"));
        photoWithTags2.getTags().add(new Tag("fourthTag"));

        photoService.addPhoto(photoWithTags2);

        final Photo photoWithTags3 = new Photo("testPhotoWithTags3");
        photoWithTags3.getTags().add(new Tag("firstTag"));
        photoWithTags3.getTags().add(new Tag("fourthTag"));

        photoService.addPhoto(photoWithTags3);

        final Photo photoWithoutTags = new Photo("testPhotoWithoutTags");
        photoService.addPhoto(photoWithoutTags);

        // When
        final Page<Photo> result =
                photoService.findPhotos(new PageRequest(0, 100), new SearchParameters(new Tag("firstTag")));

        // Then
        final Iterator<Photo> iterator = result.iterator();
        Assert.assertEquals(2, result.getTotalElements());
        Assert.assertEquals(photoWithTags1.getName(), iterator.next().getName());
        Assert.assertEquals(photoWithTags3.getName(), iterator.next().getName());
    }
    
    @Test
    public void findTags() {
        tagRepository.deleteAll();
        // Given
        tagRepository.save(new TagEntity("tag1"));
        tagRepository.save(new TagEntity("tag2"));
        tagRepository.save(new TagEntity("tag3"));

        // When
        final List<Tag> result = photoService.getAllPhotoTags();

        // Then
        Assert.assertEquals(3, result.size());
    }
}
