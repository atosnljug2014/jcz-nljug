package net.atos.jcz.nljug.rest;

/**
 * @author Atos
 */
public class RestObject {

    private String resource;


    public String getResource() {
        return resource;
    }

    public void setResource(final String resource) {
        this.resource = resource;
    }
}
