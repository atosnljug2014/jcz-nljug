package net.atos.jcz.nljug;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;

//The @ComponentScan annotation tells Spring to search recursively through the current package and
// its children for classes marked directly or indirectly with Spring’s @Component annotation.
// This directive ensures that Spring finds and registers the FotoController,
// because it is marked with @RestController, which in turn is a kind of @Component annotation.

@ComponentScan
//The @EnableAutoConfiguration annotation switches on reasonable default behaviors based on the content of your classpath.
// For example, because the application depends on the embeddable version of Tomcat (tomcat-embed-core.jar), a Tomcat server
// is set up and configured with reasonable defaults on your behalf. And because the application also depends on Spring
// MVC (spring-webmvc.jar), a Spring MVC DispatcherServlet is configured and registered for you — no web.xml necessary!
// Auto-configuration is a powerful, flexible mechanism.
@EnableAutoConfiguration

// tags the class as a source of bean definitions for the application context.
@Configuration

// Enabling the whole repository via REST using spring-boot-starter-data-rest
@EnableMongoRepositories
// Annotation imports a collection of Spring MVC controllers, JSON converters, and other beans needed to provide a RESTful
// front end. These components link up to the Spring Data JPA backend.
@Import(RepositoryRestMvcConfiguration.class)
/**
 * Spring boot application.
 *
 * @author Atos
 */
public class Application {


    /**
     * The main() method uses Spring Boot’s SpringApplication.run() method to launch an application. Did you notice that there wasn’t a single line of XML?
     * No web.xml file either. This web application is 100% pure Java and you didn’t have to deal with configuring any plumbing or infrastructure.
     *
     * @param args
     * @throws Exception
     */
    public static void main(final String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }

}
