package net.atos.jcz.nljug.rest.hello;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Just another hello world example.
 *
 * @author Atos
 */
@EnableAutoConfiguration
@RestController // combines @Controller and @ResponseBody
@RequestMapping("/hello-world")
public class HelloController {

    @RequestMapping(method = RequestMethod.GET)
    String helloWorld() {
        return "Hello World!";
    }

    @RequestMapping(value = "/{name}", method = RequestMethod.GET)
    String helloWorld(@PathVariable final String name) {
        return "Hello " + name + "!";
    }

    @RequestMapping(value = "greeting", method = RequestMethod.POST)
    ResponseEntity<Greeting> bounce(@RequestBody final Greeting greeting) {
        return new ResponseEntity<>(greeting, HttpStatus.OK);
    }
}
