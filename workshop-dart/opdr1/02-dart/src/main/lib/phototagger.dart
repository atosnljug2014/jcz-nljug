library phototagger;

import 'dart:html';
import 'dart:convert';
import 'dart:async' show Future;

import 'package:angular/angular.dart';
import 'package:angular/application_factory.dart';
import 'package:angular/routing/module.dart';
import 'package:http/browser_client.dart';
import 'package:http/http.dart' as http;

part 'components/thumbnails_component.dart';

part 'models/thumbnail.dart';

part 'services/photo_repository.dart';

class PhotoTagApp extends Module {
  PhotoTagApp() {
    bind(ThumbnailsComponent);
    bind(PhotoRepository);
    bind(UrlRewriter, toImplementation: PhototaggerUrlRewriter);
  }
}

/**
 * Deze class zorgt ervoor dat alle referenties in componenten naar een html file van de vorm "templateUrl: 'lib/components/thumbnails_component.html'"
 * herschreven worden naar "packages/phototagger/components/thumbnails_component.html". Dit is nl. de locatie @runtime waar deze bestanden zich bevinden.
 * De naam 'phototagger' wordt hierin bepaald door de naam die je in de pubspec.yaml aan je project hebt gegeven.
 */
@Injectable()
class PhototaggerUrlRewriter implements UrlRewriter {
  String call(url) => url.startsWith('lib/') ? 'packages/phototagger/${url.substring(4)}' : url;
}