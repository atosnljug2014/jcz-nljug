part of phototagger;

/**
 * Class which represents a thumbnail
 */
class Thumbnail {

  String id;

  String name;

  Thumbnail(this.id, this.name);

  String toString() => "${name}";
}