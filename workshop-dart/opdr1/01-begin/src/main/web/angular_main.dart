import 'package:angular/application_factory.dart';
import 'package:phototagger/phototagger.dart';
import 'dart:html';

main() {
  print("Starting...");
  var title = querySelector("#title");
  title.text = "Hello Angular Dart world!";

  final inj = applicationFactory().addModule(new PhotoTagApp()).run();
}

