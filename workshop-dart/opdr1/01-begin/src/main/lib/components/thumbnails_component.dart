part of phototagger;

@Component(selector: 'thumbnails',
          publishAs: 'thumbnailsComp',
        templateUrl: 'lib/components/thumbnails_component.html',
       useShadowDom: false)
class ThumbnailsComponent {

  /*** start private variables ***/
  List<Thumbnail> _thumbnails = new List();
  PhotoRepository _photoRepository;
  /*** end private variables ***/

  /**
   * Constructor which will set the photorepository and loads the first batch of thumbnails
   *
   * @param repo the photorepository
   */
  ThumbnailsComponent(PhotoRepository repo) {
    this._photoRepository = repo;
    _loadThumbnails();
  }

  /**
   * Loads the thumbnails
   */
  void _loadThumbnails() {
    print("start loading thumbnails...");
    _photoRepository.thumbnails()
      .then((thumbnails) {
        _thumbnails.addAll(thumbnails);
        print("done loading thumbnails");
      }).catchError(handleError);
  }

  void handleError(Exception e) {
    print("Error occurred while loading thumbnail data: $e");
  }

  /*** start getters and setters ***/
  List<Thumbnail> get thumbnails => _thumbnails;
  /*** end getters and setters ***/

}

