part of phototagger;

@Injectable()
class PhotoRepository {

  static String _dataLocal = "lib/data/thumbnails.json";

  Http _http;

  PhotoRepository(this._http);

  Future<List<Thumbnail>> thumbnails() {
    return _http.get(_dataLocal)
      .then((HttpResponse response) => response.data)
      .then((responseData) => responseData.map(_parseThumbnail).toList());
  }

  Thumbnail _parseThumbnail(map) => new Thumbnail(map["id"], map["name"]);

}