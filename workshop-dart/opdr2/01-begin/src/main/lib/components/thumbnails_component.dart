part of phototagger;

@Component(selector: 'thumbnails',
           publishAs: 'thumbnailsComp',
           templateUrl: 'lib/components/thumbnails_component.html',
           useShadowDom: false)
class ThumbnailsComponent {

  /*** start private variables ***/
  List<Thumbnail> _thumbnails = new List();

  PhotoService _photoService;
  /*** end private variables ***/

  /**
   * Constructor which will load the thumbnails
   *
   * @param service the photo service
   */
  ThumbnailsComponent(PhotoService service) {
    this._photoService = service;
    _loadThumbnails();
  }

  /**
   * Loads the thumbnails
   * @param filter: optional parameter with default value
   */
  void _loadThumbnails() {
    print("loading thumbnails...");
    _photoService.thumbnails()
      .then((thumbnails) {
        _thumbnails.addAll(thumbnails);
        print("done loading thumbnails");
      }).catchError(handleError);
  }

  /** Prints an error message to the console */
  void handleError(Exception e) {
    print("Error occurred when loading photos: $e");
  }

  void toggleTags() {
    DivElement rightContainer = querySelector('.rightContainer');
    DivElement leftContainer = querySelector('.leftContainer');
    rightContainer.classes.toggle('rightContainerSmall');
    leftContainer.classes.toggle('leftContainerShow');
  }

  /*** start getters and setters ***/
  List<Thumbnail> get thumbnails => _thumbnails;

  String get baseURL => Constants.PHOTO_SERVICE_BASE_URL;
  /*** end getters and setters ***/

}
