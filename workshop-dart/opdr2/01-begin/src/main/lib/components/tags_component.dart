part of phototagger;

class TagsComponent {

  /*** start variables ***/
  List<String> tags = new List();
  /*** end variables ***/

  /**
   * Constructor which will load the tags
   */
  TagsComponent() {
    print("loading tags...");
    tags.add("Tag 1");
    tags.add("Tag 2");
    tags.add("Tag 3");
  }

}
