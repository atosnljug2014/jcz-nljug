part of phototagger;

void phototaggerRouteInitializer(Router router, RouteViewFactory view) {
  view.configure({
      'tags' : ngRoute(
          path: '/tags',
          viewHtml: '<tags></tags>'
      ),
      'upload' : ngRoute(
          path: '/upload',
          viewHtml: '<uploader />'
      ),
      'thumbnails' : ngRoute(
          path: '/thumbnails',
          viewHtml: '<thumbnails></thumbnails>',
          defaultRoute: true,
          mount: {
              'tags' : ngRoute(
                  path: '/tags',
                  viewHtml: '<tags></tags>',
                  defaultRoute: true
              )
          }
      ),
      'photo' : ngRoute(
          path: '/photo/:photoId',
          viewHtml: '<photo />'
      )
  });
}
