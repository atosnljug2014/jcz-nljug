part of phototagger;

class Thumbnail {

  String id;

  String name;

  Thumbnail(this.id, this.name);

  String toString() => "${name}";
}