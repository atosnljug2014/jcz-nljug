part of phototagger;

@Component(
    selector: 'uploader',
    publishAs: 'uploadComp',
    templateUrl: 'lib/components/upload_component.html',
    useShadowDom: false)
class UploadComponent {

  /*** start private variables ***/
  PhotoService _photoService;

  UploadComponent(this._photoService);
  /*** end private variables ***/

  /**
   * Uploads the photo from the form to the server and resets the form
   */
  void uploadPhotos() {
    FormElement formElement = querySelector("#form");
    InputElement uploadInput = querySelector('#photocontent');

    final files = uploadInput.files;

    for(var file in files) {
      FormData data = new FormData();
      data.append('name', file.name);
      data.append('content', file);
      _photoService.uploadPhoto(data);
    }

    formElement.reset();
  }

}

