part of phototagger;

@Component(selector: 'photo',
          publishAs: 'photoComp',
        templateUrl: 'lib/components/photo_component.html',
       useShadowDom: false)
class PhotoComponent {

  /*** start constants ***/
  static const String ERROR_MESSAGE = "Sorry! Loading the photo failed";
  static const String LOADING_MESSAGE = "Loading...";
  /*** end constants ***/

  /*** start private variables ***/
  PhotoService _photoService;
  RouteProvider _routeProvider;
  Photo _photo;
  String _message = LOADING_MESSAGE;
  String tag;
  /*** end private variables ***/

  /**
   * Constructor which will load the photo
   * @param service the photo service
   * @param routeProvider the routeProvider
   */
  PhotoComponent(PhotoService service, RouteProvider routeProvider) {
    _photoService = service;
    _routeProvider = routeProvider;
    _loadPhoto();
  }

  /**
   * Loads the photo from the photo service
   */
  _loadPhoto() {
    _photoService.photo(photoId).then((photo) {
      _photo = photo;
    }).catchError((e) {
      _message = e;
    });
  }

  /**
   * Get id of photo from routing provider
   */
  String get photoId => _routeProvider.parameters["photoId"];

  /**
   * Add tag to photo
   */
  void addTag() {
    _photoService.tagPhoto(photoId, tag);
    photo.tags.add(tag);
    tag = '';
  }

  /*** start getters and setters ***/

  Photo get photo => _photo;

  String get message => _message;

  String get serverUrl => Constants.PHOTO_SERVICE_BASE_URL;

  /*** end getters and setters ***/

}

