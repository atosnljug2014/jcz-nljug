part of phototagger;

@Component(selector: 'tags',
           publishAs: 'tagsComp',
           templateUrl: 'lib/components/tags_component.html',
           useShadowDom: false)
class TagsComponent {

  /*** start private variables ***/
  List<Tag> _tags = new List();

  PhotoService _photoService;

  ThumbnailsComponent _thumbnailsComponent;

  String _nameFilterString = "";

  List<String> _tagNames = [];
  /*** end private variables ***/

  /**
   * Constructor which will load the tags
   *
   * @param service the photo service
   * @param thumbnailsComponent the thumbnails component
   */
  TagsComponent(PhotoService service, ThumbnailsComponent thumbnailsComponent) {
    this._photoService = service;
    this._thumbnailsComponent = thumbnailsComponent;
    _loadTags();
  }

  /**
   * Loads the tags from the photo service
   */
  _loadTags() {
    print("loading tags...");
    _photoService.tags()
      .then((List tags) => _tags.addAll(tags))
      .then((List tags) => print("done loading tags"))
      .catchError((Exception e) => print("Error occurred while loading tags: $e"));
  }

  //OPDRACHTMARKER - aanmaken filter methode
  /**
   * filters the thumbnails based on the selected tags and keeps track of the previous selected tags
   * @param tag - the selected tag
   */
  filter(Tag tag) {
    if (tag.selected) {
      _tagNames.remove(tag.name);
      tag.selected = false;
    } else {
      _tagNames.add(tag.name);
      tag.selected = true;
    }
    _thumbnailsComponent._loadThumbnails(tagfilter: _tagNames);
  }

  /*** start getters and setters ***/
  List<Tag> get tags => _tags;

  String get nameFilterString => _nameFilterString;

  void set nameFilterString(String nameFilterString) {
    _nameFilterString = nameFilterString;
  }
  /*** end getters and setters ***/

}
