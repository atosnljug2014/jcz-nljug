import 'package:angular/application_factory.dart';
import 'package:phototagger/phototagger.dart';

main() {
  final inj = applicationFactory().addModule(new PhotoTagApp()).run();
}

