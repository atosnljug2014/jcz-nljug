part of phototagger;

class Photo {

  String id;

  String name;

  List<String> tags;

  DateTime date;

  Photo(this.id, this.name, this.tags, this.date);

  String toString() => "${name}";

}